import React, { useState } from 'react';
import logo from '../img/hbsis.png';
import './Cadastro.css';

import api from '../services/api';

export default function Cadastro({history}) {
    const [Nome, setNome] = useState('');
    const [Login, setlogin] = useState('');
    const [Senha, setSenha] = useState('');
    const [Email, setEmail] = useState(''); 

    async function Cadastrar(e) {
        e.preventDefault();
        try{
            await api.post('/usuario', { 
                Nome,
                Login,
                Senha,
                Email,
            });
            history.push(''); 
        } catch(response) {
            alert(response.response.data.detailedMessage);
        }
    }        
              
    return (        
        <div className="cadastro-container"> 
        <form onSubmit={Cadastrar}>
            <img src={logo} alt="Logo da HBSIS."/>
            <input name="Nome" placeholder="Informe seu nome completo." value={Nome} onChange={e => setNome(e.target.value)}/>
            <input name="Login" placeholder="Informe um nome de usuário." value={Login} onChange={e => setlogin(e.target.value)}/>
            <input name="Senha" type="password" placeholder="Informe uma senha." value={Senha} onChange={e => setSenha(e.target.value)} />
            <input name="Email" placeholder="Informe seu email." value={Email} onChange={e => setEmail(e.target.value)}/>        
            <button type="submit">Cadastrar</button>
        </form>          
        </div>
    );
}
          
